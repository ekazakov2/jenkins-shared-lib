#!/usr/bin/env groovy

package com.example

class Docker implements Serializable {
    def script

    Docker(script) {
        this.script = script
    }

    // vissper/java-maven-app:1.3'
    def buildDockerImage(String imageName) {
        script.echo 'building the docker image...'
        script.sh "docker build -t $imageName ."  // nexus.ekazakov.xyz:8083
    }

    def dockerLogin() {
        script.withCredentials(
            [script.usernamePassword(
                credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER'
            )]
        ) {
            script.sh 'echo "$PASS" | docker login -u "$USER" --password-stdin' //nexus.ekazakov.xyz:8083
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName" // nexus.ekazakov.xyz:8083
    }
}