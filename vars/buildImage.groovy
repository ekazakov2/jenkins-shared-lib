#!/usr/bin/env groovy

import com.example.Docker

// vissper/java-maven-app:1.3
def call(String imageName) {
    Docker docker = new Docker(this)

    docker.dockerLogin()
    docker.buildDockerImage(imageName)
    docker.dockerPush(imageName)
    //  echo 'building the docker image...'
    //  withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
    //       sh 'docker build -t vissper/java-maven-app:1.3 .'  // nexus.ekazakov.xyz:8083
    //       sh 'echo "$PASS" | docker login -u "$USER" --password-stdin' //nexus.ekazakov.xyz:8083
    //       sh 'docker push vissper/java-maven-app:1.3' // nexus.ekazakov.xyz:8083
    //  }
}
